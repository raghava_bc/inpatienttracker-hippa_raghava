import { Component } from '@angular/core';
import { Events, NavController, NavParams } from 'ionic-angular';
import { SubscriptionHelperSerivce } from '../../providers/helper-service';
import { StorageService } from '../../../../providers/storage-service';
declare var Stripe;
import { Storage } from '@ionic/storage';

@Component({
  selector: 'payment-add-card',
  templateUrl: 'payment-add-card.html',
})
export class PaymentAddCard {

  stripe = Stripe('pk_test_RYU23lnECbxAWuO8PUW6P7w8');
  card: any;
  user: any = {};
  stripeCardResponse: any = {};
  stripeCustId: any;
  chargeCustomer: boolean = false;
  stripePlanId: string;
  successCallback: any;
  isTrialRequired: any;

  constructor(
    public events: Events,
    public helper: SubscriptionHelperSerivce,
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    public storageService: StorageService
  ) {
    this.chargeCustomer = this.navParams.get('chargeCustomer');
    this.stripePlanId = this.navParams.get('stripePlanId');
    this.successCallback = this.navParams.get('successCallback');
  }

  ionViewDidLoad() {
    this.setupStripe();
  }

  setupStripe() {
    this.helper.showLoading();
    let elements = this.stripe.elements();
    var style = {
      base: {
        color: '#32325d',
        lineHeight: '24px',
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSmoothing: 'antialiased',
        fontSize: '16px',
        '::placeholder': {
          color: '#aab7c4'
        }
      },
      invalid: {
        color: '#fa755a',
        iconColor: '#fa755a'
      }
    };

    this.card = elements.create('card', { style: style });

    this.card.mount('#card-element');
    this.storageService.getSettings('STRIPE_CUST_ID')
      .then((stripeCustId: any) => {
        this.stripeCustId = stripeCustId;
      })
    this.card.addEventListener('change', event => {
      var displayError = document.getElementById('card-errors');
      if (event.error) {
        displayError.textContent = event.error.message;
      } else {
        displayError.textContent = '';
      }
    });
    this.helper.hideLoading();

    var form = document.getElementById('payment-form');
    form.addEventListener('submit', event => {
      event.preventDefault();
      this.card.flow = 'code_verification';
      this.stripe.createSource(this.card)
        .then(result => {
          if (result.error) {
            var errorElement = document.getElementById('card-errors');
            errorElement.textContent = result.error.message;
          } else {
            this.stripeCardResponse = result;
            this.onsubmit();
          }
        })
        .catch((err) => {
          console.log('err occured', err)
        });
    });
  }

  onsubmit() {
    let self: any = this;
    new Promise((resolve, reject) => {
      this.helper.showLoading();
      if (this.stripeCustId && this.stripeCustId !== 'null') {
        resolve();
      }
      else {
        this.createStripeCustomer()
          .then((response: any) => {
            resolve(response);
          })
          .catch((err) => {
            reject(err);
          });
      }
    })
      .then((response: any) => {
        let customer;
        console.log('response', response)
        if (!this.chargeCustomer) {
          this.attachSource()
            .then(() => {
              self.helper.hideLoading();
              self.helper.showMessage('successfully added the card');
              self.navCtrl.pop()
                .then(() => {
                  self.events.publish('refresh:manage-sources')
                });
            })
            .catch((err) => {
              console.log('err occured', err);
              this.helper.hideLoading();
              let msg: any;
              if (err && err._body && JSON.parse(err._body) && JSON.parse(err._body).error) {
                let errorObj = JSON.parse(err._body).error
                msg = errorObj.message;
              }
              else {
                msg = 'Failed to add card, please try after some time';
              }
              this.helper.showMessage(msg, 3000);
            })
        }
        else {
          if (response._body && JSON.parse(response._body).statusCode != 200) {
            this.helper.hideLoading();
            this.helper.showMessage(JSON.parse(response._body).body, 3000);
          }
          else {
            if (response && response._body && JSON.parse(response._body).body && JSON.parse(JSON.parse(response._body).body) && JSON.parse(JSON.parse(response._body).body).customer) {
              customer = JSON.parse(JSON.parse(response._body).body).customer;
            }
            if (customer && customer.id) {
              self.storageService.setSettings('STRIPE_CUST_ID', customer.id);
              self.stripeCustId = customer.id;
              self.storageService.getSettings('TRIAL_REQUIRED')
                .then((trialRequired: any) => {
                  self.isTrialRequired = trialRequired;
                  let obj: any = { customerId: self.stripeCustId, planId: self.stripePlanId };
                  if (!(self.isTrialRequired && (self.isTrialRequired == 'null' || self.isTrialRequired == false || self.isTrialRequired == 'false'))) {
                    obj.trial_from_plan = true;
                  }
                  return self.helper.createStripeCharges(obj);
                })
                .then((response: any) => {
                  if (response._body && JSON.parse(response._body).statusCode != 200) {
                    this.helper.hideLoading();
                    this.helper.showMessage(JSON.parse(response._body).body, 3000);
                  }
                  else if (response._body && JSON.parse(response._body).body && JSON.parse(JSON.parse(response._body).body)) {
                    self.storageService.setSettings('DEFAULT_SOURCE_ID', self.stripeCardResponse.source.id)
                      .then(() => {
                        return self.storageService.setSettings('TRIAL_REQUIRED', 'false');
                      })
                      .then(() => {
                        self.helper.hideLoading();
                        self.helper.showMessage('successfully subscribed to subscription');
                        return self.navCtrl.pop();
                      })
                      .then(() => {
                        self.events.publish('refresh:subscription-listing');
                      })
                      .catch((err) => {
                        self.helper.hideLoading();
                        console.log('err occured', err)
                      })
                  }
                })
                .catch((err) => {
                  self.helper.hideLoading();
                  console.log('err occured', err)
                })
            }
            else {
              self.helper.hideLoading();
            }
          }

        }
      })
      .catch((err) => {
        console.log('err occured', err);
        self.helper.hideLoading();
      })
  }

  createStripeCustomer() {
    return new Promise(async (resolve, reject) => {
      let uid = await this.storage.get('uid');
      this.helper.createStripeCustomer({ email: this.user.email, name: this.user.name, sourceId: this.stripeCardResponse.source.id, userId: uid })
        .then((response: any) => {
          resolve(response)
        })
        .catch((err: any) => {
          console.log('err occured', err);
          reject(err);
        })
    })
  }

  attachSource() {
    return new Promise((resolve, reject) => {
      this.helper.attachSource(this.stripeCustId, this.stripeCardResponse.source.id)
        .then((response: any) => {
          resolve(response)
        })
        .catch((err: any) => {
          console.log('err occured', err);
          reject(err);
        })
    })
  }
}
