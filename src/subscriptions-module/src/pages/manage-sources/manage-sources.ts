import { Component } from '@angular/core';
import { AlertController, Events, NavController } from 'ionic-angular';
import { SubscriptionHelperSerivce } from '../../providers/helper-service';
import { PaymentAddCard } from '../payment-add-card/payment-add-card';
import { CardDetail } from './card-detail/card-detail';
import { StorageService } from '../../../../providers/storage-service';
import * as _ from 'underscore';
import * as moment from 'moment';

@Component({
  selector: 'manage-sources',
  templateUrl: 'manage-sources.html'
})

export class ManageSources {
  subscriptions: any[] = [];
  isSubscribed: boolean = false;
  stripeCustId: any;
  isReady: boolean = false;
  currentSubscribedPlan: any = {};
  moment = moment;
  subscriptionId: any = '';
  subscriptionPlanId: any = '';
  customerSources: any;
  deafultSourceId: any;
  sourceId: any;
  eventSubscription: any;
  constructor(
    public alertCtrl: AlertController,
    public events: Events,
    public helper: SubscriptionHelperSerivce,
    public navCtrl: NavController,
    public storageService: StorageService
  ) {
    this.eventSubscription = this.events.subscribe('refresh:manage-sources', () => {
      this.init();
      if (this.eventSubscription) {
        this.eventSubscription.unsubscribe();
      }
    })
  }

  ionViewDidLoad() {
    this.init();
  }

  init() {
    let self: any = this;
    this.helper.hideLoading();
    this.helper.showLoading();
    this.storageService.getSettings('STRIPE_CUST_ID')
      .then((stripeCustId: any) => {
        if (stripeCustId) {
          this.stripeCustId = stripeCustId;
          self.storageService.getSettings('DEFAULT_SOURCE_ID')
            .then((defaualtSourceId) => {
              self.deafultSourceId = defaualtSourceId;
              return self.helper.getCustomerSources(stripeCustId);
            })
            .then((response: any) => {
              if (JSON.parse(response._body) && JSON.parse(response._body).data) {
                this.customerSources = JSON.parse(response._body).data;
                this.helper.hideLoading();
              }
              else {
                this.helper.hideLoading();
              }
            })
        }
        else {
          this.helper.hideLoading();
        }
      })
      .catch((err) => {
        console.log('err occured', err)
        this.helper.hideLoading();
      })
  }

  viewCardDetail(card: any, deafultSourceId:any) {
    this.navCtrl.push(CardDetail, { card: card, deafultSourceId:deafultSourceId });
  }

  addSource() {
    this.navCtrl.push(PaymentAddCard, { successCallback: this });
  }
}
