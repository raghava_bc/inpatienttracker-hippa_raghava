import { Component } from '@angular/core';
import { AlertController, Events, NavController, NavParams } from 'ionic-angular';
import { SubscriptionHelperSerivce } from '../../../providers/helper-service';
import { StorageService } from '../../../../../providers/storage-service';
import * as _ from 'underscore';
import * as moment from 'moment';

@Component({
  selector: 'card-detail',
  templateUrl: 'card-detail.html'
})

export class CardDetail {

  subscriptions: any[] = [];
  isSubscribed: boolean = false;
  stripeCustId: any;
  isReady: boolean = false;
  currentSubscribedPlan: any = {};
  moment = moment;
  subscriptionPlanId: any = '';
  customerSources: any;
  source: any = {};
  deafultSourceId: any;

  constructor(
    public alertCtrl: AlertController,
    public events: Events,
    public helper: SubscriptionHelperSerivce,
    public navCtrl: NavController,
    public navParams: NavParams,
    public storageService: StorageService
  ) {
    this.source = this.navParams.get('card');
    this.deafultSourceId = this.navParams.get('deafultSourceId');
    this.init();
    this.helper.getsubscriptionPlans()
      .then((response: any) => {
        console.log('response', response)
        console.log('JSON.parse(response._body)', JSON.parse(response._body))
        if (JSON.parse(response._body) && JSON.parse(response._body).data) {
          this.subscriptions = JSON.parse(response._body).data;
        }
      })
      .catch((err) => {
        console.log('eror occured', err);
      })
  }

  init() {
    let self: any = this;
    this.helper.hideLoading();
    this.helper.showLoading();
    this.storageService.getSettings('STRIPE_CUST_ID')
      .then((stripeCustId: any) => {
        this.stripeCustId = stripeCustId;
        this.helper.hideLoading();
      })
      .catch((err) => {
        this.helper.hideLoading();
        console.log('err occured', err)
      })
  }


  showAlertForDeleteCard() {
    this.alertCtrl.create({
      title: 'Delete Payment card',
      message: 'Are you sure want to delete this card',
      buttons: [
        {
          text: 'Cancel'
        },
        {
          text: 'Proceed',
          handler: () => {
            this.deleteSource();
          }
        }
      ]
    }).present();
  }

  deleteSource() {
    this.helper.showLoading();
    this.helper.detachCardSource(this.stripeCustId, this.source.id)
      .then((response: any) => {
        this.helper.hideLoading();
        this.events.publish('refresh:manage-sources');
        this.navCtrl.pop();
      })
      .catch((err) => {
        console.log('err occured', err);
        this.helper.hideLoading();
      })
  }

  changeDefaultSource() {
    if (this.stripeCustId && this.source.id) {
      this.helper.showLoading();
      this.helper.changeDefaultSource(this.stripeCustId, this.source.id)
        .then((response: any) => {
          return this.storageService.setSettings('DEFAULT_SOURCE_ID', this.source.id);
        })
        .then(() => {
          this.helper.hideLoading();
          this.helper.showMessage('default source changed successfully');
          this.events.publish('refresh:manage-sources');
          this.navCtrl.pop();
        })
        .catch((err) => {
          this.helper.hideLoading();
          this.helper.showMessage('failed to change the default source')
          console.log('err occured', err)
        })
    }
  }
}
