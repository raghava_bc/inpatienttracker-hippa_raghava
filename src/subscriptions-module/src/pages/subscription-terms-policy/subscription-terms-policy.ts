import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-subscription-terms',
  templateUrl: 'subscription-terms-policy.html'
})

export class SubscriptionTermsPage {
  subscriptionData: any;

  constructor(
    public navParams: NavParams,
    public viewCtrl: ViewController
  ) {
    this.subscriptionData = this.navParams.get('subscriptionData')
  }

  agree() {
    this.viewCtrl.dismiss({ agreed: true, subscriptionData: this.subscriptionData });
  }
}
