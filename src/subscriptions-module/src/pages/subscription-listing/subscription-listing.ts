import { Component } from '@angular/core';
import { Events, MenuController, ModalController, NavController, NavParams } from 'ionic-angular';
import { SubscriptionHelperSerivce } from '../../providers/helper-service';
import { PaymentAddCard } from '../payment-add-card/payment-add-card';
import { StorageService } from '../../../../providers/storage-service';
import * as _ from 'underscore';
import { ManageSources } from '../manage-sources/manage-sources';
import { SubscriptionTermsPage } from '../subscription-terms-policy/subscription-terms-policy';

@Component({
  selector: 'subscription-listing',
  templateUrl: 'subscription-listing.html'
})

export class SubscriptionListing {
  subscriptions: any[] = [];
  isSubscribed: boolean = false;
  stripeCustId: any;
  isReady: boolean = false;
  currentSubscribedPlan: any = {};
  eventSubscription: any;
  isTrialRequired: any;
  isWelcomeScreen: boolean;

  constructor(
    public events: Events,
    public helper: SubscriptionHelperSerivce,
    public menuCtrl: MenuController,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public storageService: StorageService
  ) {
    this.isWelcomeScreen = this.navParams.get('isWelcomeScreen') || false;
    this.menuCtrl.swipeEnable(false);
    this.menuCtrl.enable(false);
    this.events.subscribe('refresh:subscription-listing', () => {
      this.init();
      if (this.eventSubscription) {
        this.eventSubscription.unsubscribe();
      }
    })
  }

  ionViewDidLoad() {
    this.init();
  }

  init() {
    this.helper.showLoading();
    let self: any = this;
    this.storageService.getSettings('STRIPE_CUST_ID')
      .then((stripeCustId: any) => {
        let isTestEnvironment = localStorage.getItem('TEST_ENVIRONMENT');
        if (stripeCustId && stripeCustId !== 'null') {
          this.stripeCustId = stripeCustId;
          this.getSubscribedPlans();
        }
        else if (isTestEnvironment || isTestEnvironment == 'true') {
          this.isSubscribed = true;
          this.isReady = true;
          this.helper.hideLoading();
        }
        else {
          this.isReady = true;
          this.isSubscribed = false;
          this.helper.hideLoading();
        }
      })
      .catch((err) => {
        console.log('err occured', err);
        this.helper.hideLoading();
      })
    this.helper.getsubscriptionPlans()
      .then((response: any) => {
        if (JSON.parse(response._body) && JSON.parse(response._body).data) {
          this.subscriptions = JSON.parse(response._body).data;
        }
      })
      .catch((err) => {
        console.log('eror occured', err);
      })
  }

  getSubscribedPlans() {
    this.helper.getSubscribedPlans(this.stripeCustId)
      .then((response: any) => {
        if (JSON.parse(response._body) && JSON.parse(response._body).data) {
          let customerSubscriptions = JSON.parse(response._body).data;
          this.currentSubscribedPlan = _.find(customerSubscriptions, (sub: any) => {
            return sub.status == 'trialing' || sub.status == 'active';
          });
          if (this.currentSubscribedPlan) {
            this.isReady = this.isSubscribed = true;
            this.helper.hideLoading();
          }
          else {
            this.isReady = true;
            this.isSubscribed = false;
            this.helper.hideLoading();
          }
        }
      })
      .catch((err: any) => {
        console.log('Err occured', err);
        this.helper.hideLoading();
      })
  }

  subscribe(subscriptionData: any) {
    let self: any = this;
    if (this.stripeCustId) {
      this.helper.showLoading();
      this.storageService.getSettings('TRIAL_REQUIRED')
        .then((trialRequired: any) => {
          self.isTrialRequired = trialRequired;
          let obj: any = { customerId: this.stripeCustId, planId: subscriptionData.id };
          if (!(self.isTrialRequired && (self.isTrialRequired == 'null' || self.isTrialRequired == false || self.isTrialRequired == 'false'))) {
            obj.trial_from_plan = true;
          }
          return this.helper.createStripeCharges(obj)
        })
        .then((response: any) => {
          if (response._body && JSON.parse(response._body).statusCode != 200) {
            this.getSubscribedPlans();
            this.helper.hideLoading();
            this.helper.showMessage(JSON.parse(response._body).body, 3000);
          }
          else if (response._body && JSON.parse(response._body).body && JSON.parse(JSON.parse(response._body).body)) {
            this.storageService.setSettings('TRIAL_REQUIRED', 'false')
              .then(() => {
                this.getSubscribedPlans();
                self.helper.hideLoading();
                self.helper.showMessage('successfully subscribed to the subscription');
              })
          }
        })
        .catch((err) => {
          this.helper.hideLoading();
          this.helper.showMessage('Failed to subscribe to the subscription');
          console.log('err occured', err)
        })
    } else {
      this.navCtrl.push(PaymentAddCard, { chargeCustomer: true, stripePlanId: subscriptionData.id, successCallback: this });
    }
  }

  viewSubscriptionTermsConditonsPage(subscriptionData: any) {
    let self: any = this,
      modal = this.modalCtrl.create(SubscriptionTermsPage, { subscriptionData: subscriptionData });
    modal.present();
    modal.onDidDismiss((response: any) => {
      if (response && response.agreed) {
        self.subscribe(response.subscriptionData);
      }
    })
  }

  pop() {
    this.navCtrl.pop();
  }

  viewManageCards() {
    this.navCtrl.push(ManageSources);
  }

  finish() {
    this.events.publish('user:app-ready');
  }
}
