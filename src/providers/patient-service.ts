import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Apollo } from 'apollo-angular';
import { ApolloService } from '../providers/apollo-service';
import { AlertController, ActionSheetController, Content, LoadingController, Platform, Select } from "ionic-angular";
import { AddNewPatientForDr, GetActivePatientForDr, GetCartPatient, GetCartPatientEntities, getPatientDetailForDr, GetActivePatient, MarkVisitedPatient, AddNewPatient, UpdatePatient, updatePatientDetailForDr, DeletePatient } from '../queries/patient-info';
import { GetArchivedPatients } from '../queries/archives';
import { hospitals } from '../queries/hospitals';
import { Helper } from '../providers/helper';
import { subscriptionToHospitals } from '../queries/subscription-to-hospitals';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import 'rxjs/add/operator/take';
import * as _ from 'underscore';

import {
  GetNotes, CreateNotes, DeleteNotes, UpdateNotes, getTopLookups,
  addNewEntities, addNewEntitiesForDr, updateEntities, updateEntitiesForDr,
  getPatientEntities, getPatientEntitiesForDr, deleteAllEntities,
  createFavouriteEntities, getUserFavoriteCodes, deleteFavouriteEntities,
  updateFavouriteEntities, updateMultipleFavEntities, createRecordsForBilling,
  upsertRecordsForBilling, searchIcdCodes, searchCptCodes, queryBillingsRecordsForSecretary
} from '../queries/entities';

@Injectable()
export class PatientService {
  topLookupCodes: any = new BehaviorSubject([]);
  userFavouriteCodes: any = new BehaviorSubject([]);
  constructor(
    private apollo: Apollo,
    public apolloService: ApolloService,
  ) {
  }

  getActivePatients(queryRef: any, hospitalId: String) {
    return this.apolloService.fetch(queryRef, GetActivePatient, { hospitalId: hospitalId });
  }

  getActivePatientForDr(queryRef: any, hospitalId: String, connectedUserAlias:String) {
    return this.apolloService.fetch(queryRef, GetActivePatientForDr, { hospitalId, connectedUserAlias });
  }

  getPatientDetail(hospitalId:string, patientId:string, connectedUserAlias:string, queryRef?:any){
    let ref:any;
    queryRef = queryRef || ref;
    return this.apolloService.fetch(queryRef, getPatientDetailForDr, { hospitalId:hospitalId,  patientId:patientId, connectedUserAlias:connectedUserAlias })
  }

  getInCartPatients(queryRef: any, hospitalId: String) {
    return this.apolloService.fetch(queryRef, GetCartPatient, { hospitalId: hospitalId });
  }

  getCartPatientEntities(queryRef: any, hospitalId: any, patientIds: any) {
    return this.apolloService.fetch(queryRef, GetCartPatientEntities, { hospitalId: hospitalId, patientIds: patientIds });
  }

  markVisitedPatient(patientId: any, timestamp: string, hospitalId: string) {
    return this.apolloService.mutate({ mutation: MarkVisitedPatient, variables: { "patientId": patientId, "hospitalId": hospitalId, "lastUpdated": timestamp } });
  }

  upsertPatient(patientObj) {
    return this.apolloService.mutate({ mutation: patientObj.patientId ? UpdatePatient : AddNewPatient, variables: patientObj });
  }

  upsertPatientDr(patientObj) {
    return this.apolloService.mutate({ mutation: patientObj.patientId ? updatePatientDetailForDr : AddNewPatientForDr, variables: patientObj });
  }

  deletePatient(patientId: any, hospitalId: any) {
    return this.apolloService.mutate({ mutation: DeletePatient, variables: { patientId: patientId, hospitalId: hospitalId } });
  }

  upsertPatientNotes(notesObj: any, mode?: any) {
    return this.apolloService.mutate({ mutation: mode && notesObj.id ? UpdateNotes : CreateNotes, variables: notesObj });
  }

  deletePatientNotes(notesObj) {
    return this.apolloService.mutate({ mutation: DeleteNotes, variables: notesObj });
  }

  getTopLookupsBySpecialization(queryRef, specializationId: any) {
    return this.apolloService.fetch(queryRef, getTopLookups, { specializationId: specializationId });
  }

  upsertPatientEntities(entityObj, action?) {
    return this.apolloService.mutate({ mutation: action == "CREATE" ? addNewEntities : updateEntities, variables: entityObj });
  }

  upsertPatientEntitiesForDr(entityObj, action?) {
    return this.apolloService.mutate({ mutation: action == "CREATE" ? addNewEntitiesForDr : updateEntitiesForDr, variables: entityObj });
  }

  upsertRecordsForBilling(entityObj, isUpsert?){
    return this.apolloService.mutate({ mutation: isUpsert == false ? createRecordsForBilling : upsertRecordsForBilling, variables: entityObj });
  }

  queryBillingsRecordsForSecretary(queryRef: any, userAlias: string, hospitalId: string, first?:number, after?:string){
    return this.apolloService.fetch(queryRef, queryBillingsRecordsForSecretary, { userAlias, hospitalId, first, after })
  }

  getPatientEntities(queryRef: any, patientId: string, hospitalId: string) {
    return this.apolloService.fetch(queryRef, getPatientEntities, { patientId: patientId, hospitalId: hospitalId });
  }

  getPatientEntitiesForDr(queryRef: any, patientId: string, hospitalId: string, connectedUserAlias:string) {
    return this.apolloService.fetch(queryRef, getPatientEntitiesForDr, { patientId, hospitalId, connectedUserAlias });
  }

  getArchivedPatients(queryRef: any, hospitalId: string) {
    return this.apolloService.fetch(queryRef, GetArchivedPatients, { hospitalId: hospitalId });
  }

  deletePatientEntities(entityObj) {
    return this.apolloService.mutate({ mutation: deleteAllEntities, variables: entityObj });
  }

  toggleLookupFavourites(lookupCodeObj, type) {
    return this.apolloService.mutate({ mutation: type == "CREATE" ? createFavouriteEntities : updateFavouriteEntities, variables: lookupCodeObj });
  }

  getFavouriteUserCodes(queryRef) {
    return this.apolloService.fetch(queryRef, getUserFavoriteCodes, {});
  }

  getGlobalCodes(type, queryRef, variables) {
    return this.apolloService.fetch(queryRef, type == "CPT" ? searchCptCodes : searchIcdCodes, variables);
  }

  addGlobalCodesToFavList(variables) {
    return this.apolloService.mutate({ mutation: updateMultipleFavEntities, variables: variables });
  }
}
