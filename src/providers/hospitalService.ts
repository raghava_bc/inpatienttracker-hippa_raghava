import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Apollo } from 'apollo-angular';
import { Helper } from '../providers/helper';
import { listHospitals } from '../queries/list-hospitals';
import { createUserHospitals, createMailingInfo, deleteUserHospitals, deleteMailingInfo, updateMailingInfo, getMailingInfo, subscriptionToUserHospitals, getUserHospital, updateUserHospital, updateUserHospitalDetail } from '../queries/userHospitals';
import { createRelations, getSecretaryListForHospital, getNurseListForHospital, getConnectedUserTypeList, hospitals, listMyRelations } from '../queries/hospitals';
import { listAllCustomUserHospitals } from '../queries/list-user-hospitals';

import { subscriptionToHospitals } from '../queries/subscription-to-hospitals';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import * as _ from 'underscore';
import { StorageService } from './storage-service';
import { Geolocation } from '@ionic-native/geolocation';
import { createUserRoleSettings, deleteRelations, deleteUserRoleSettings, getUserRoleSettings, getUserRoleSettingsForRelation, updateUserRoleSettings, getCustomCards, getHospitalCards, createCard, deleteCard, getApprovalPendingCards   } from '../queries/settings';
@Injectable()

export class HospitalService {

  hospitals = new BehaviorSubject([]);
  userHospitals = new BehaviorSubject([]);

  constructor(
    private apollo: Apollo,
    public geolocation: Geolocation,
    public helper: Helper,
    public storageService: StorageService
  ) { }

  initApollo() {
    this.apollo.watchQuery({ query: listHospitals, fetchPolicy: 'network-only' }).valueChanges
      .pipe(map(result => result.data['listAllHospitals'].items)).subscribe(data => {
        const copied = JSON.parse(JSON.stringify(data));
        this.hospitals.next(copied);
      });

    this.apollo.watchQuery({ query: getUserHospital, fetchPolicy: 'network-only' }).valueChanges
      .pipe(map(result => result.data['getUserHospitals'].items)).subscribe(data => {
        if (data && data.length) {
          const copied = JSON.parse(JSON.stringify(data));
          this.userHospitals.next(copied);
        }
      });

    // this.apollo.subscribe({query: subscriptionToUserHospitals}).subscribe(o => {
    //   const hospital  = o.data.onCreateUserHospitals;
    //   if (this.userHospitals.getValue()[0].findIndex(p => p.hospitalId === hospital.hospitalId) === -1) {
    //     this.userHospitals.next([...this.userHospitals.getValue()[0], hospital]);
    //     // const copied = Object.assign([], this.userHospitals.getValue()[0]);
    //     // this.userHospitals.next(copied);
    //   }
    // });

    // this.apollo.subscribe({query: subscriptionToHospitals}).subscribe(o => {
    //   const hospital  = o.data.onCreateHospitals;
    //   console.log(this.hospitals.getValue(), this.hospitals.getValue().findIndex(p => p.id === hospital.id));
    //   if (this.hospitals.getValue().findIndex(p => p.id === hospital.id) === -1) {
    //     // this.hospitals.next([...this.hospitals.getValue(), hospital]);
    //     const copied = Object.assign([], this.hospitals.getValue()[0]);
    //     this.hospitals.next(copied);
    //     console.log('hospitals ', this.hospitals.getValue()[0]);
    //   }
    // });
  }

  autoSelectHosptial() {
    return new Promise((resolve, reject) => {
      this.geolocation.getCurrentPosition({
        enableHighAccuracy: true,
        maximumAge: 0
      })
        .then(async (position) => {
          await this.storageService.setLocalSettings('CURRENT_LOCATION_LAT', position.coords.latitude)
          await this.storageService.setLocalSettings('CURRENT_LOCATION_LNG', position.coords.longitude)
          let hospitals = this.userHospitals.getValue();
          let temp = [];
          hospitals.forEach((hospital: any) => {
            if(hospital.isRemoved != 'true' && ((hospital.isCustom == true && hospital.approvalStatus == 'approved') || hospital.isCustom != "true")){
              let value = this.calcDistanceBetween(
                position.coords.latitude,
                position.coords.longitude,
                (hospital.hospitalDetail.lat ? hospital.hospitalDetail.lat : '0.00'),
                (hospital.hospitalDetail.long ? hospital.hospitalDetail.long : '0.00')
              );
              hospital.distance = value;
              temp.push(hospital)
            }
          })
          hospitals = temp;
          let nearestHospital = _.sortBy(hospitals, 'distance')[0];
          if (nearestHospital.distance <= 100) {
            await this.switchHospital(nearestHospital)
            resolve()
          } else {
            resolve()
          }
        })
        .catch((err) => {
          console.log('err occured', err)
          reject(err)
        });
    })
  }

  calcDistanceBetween(lat1, lon1, lat2, lon2) {
    //Radius of the earth in:  1.609344 miles,  6371 km  | var R = (6371 / 1.609344);
    var R = 3958.7558657440545; // Radius of earth in Miles
    var dLat = this.toRad(lat2 - lat1);
    var dLon = this.toRad(lon2 - lon1);
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this.toRad(lat1)) * Math.cos(this.toRad(lat2)) *
      Math.sin(dLon / 2) * Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return d * 1000; //returns distance in meters
  }

  toRad(Value) {
    /** Converts numeric degrees to radians */
    return Value * Math.PI / 180;
  }

  public getUserHospitals() {
    return new Promise((resolve) => {
      this.apollo.watchQuery({ query: getUserHospital, fetchPolicy: 'network-only' }).valueChanges
        .pipe(map(result => result.data['getUserHospitals'].items)).subscribe(data => {
          if (data && data.length) {
            const copied = JSON.parse(JSON.stringify(data));
            resolve(copied);
          } else {
            resolve([]);
          }
        });
    })
  }



  // Mailing List for particular hospital
  getMailingInfo(hospitalId: any) {
    return new Promise((resolve) => {
      this.apollo.watchQuery({ query: getMailingInfo, variables: { hospitalId }, fetchPolicy: 'network-only' }).valueChanges
        .pipe(map(result => result.data['getMailingInfo'])).subscribe(data => {
          resolve(JSON.parse(JSON.stringify(data)));
        });
    })
  }


  createUserHospital(hospital: any) {
    hospital = _.pick(hospital, _.identity);
    this.userHospitals.next([[...this.userHospitals.getValue(), hospital]]);
    return new Promise((resolve, reject) => {
      this.apollo.mutate({ mutation: createUserHospitals, variables: hospital })
        .subscribe(data => {
          resolve();
        }, (err => {
          console.log("Error occured in hospital creation");
          reject();
        }));
    });
  }

  updateUserHospitalDetail(hospital: any) {
    return new Promise((resolve, reject) => {
      this.apollo.mutate({ mutation: updateUserHospitalDetail, variables: hospital })
        .subscribe(data => {
          resolve();
        }, (err => {
          console.log("Error occured in hospital update", err);
          reject(err);
        }));
    });
  }

  updateUserHospital(hospital: any) {
    return new Promise((resolve, reject) => {
      this.apollo.mutate({ mutation: updateUserHospital, variables: hospital })
        .subscribe(data => {
          resolve();
        }, (err => {
          console.log("Error occured in hospital update", err);
          reject(err);
        }));
    });
  }

  removeUserHospital(hospitalId: any) {
    return new Promise((resolve, reject) => {
      this.apollo.mutate({ mutation: deleteUserHospitals, variables: { hospitalId } })
        .subscribe(data => {
          resolve();
        }, (err => {
          console.log("Error occured in removeUserHospital");
          reject();
        }));
    });
  }

  removeMailingInfo(hospitalId: any) {
    return new Promise((resolve, reject) => {
      this.apollo.mutate({ mutation: deleteMailingInfo, variables: { hospitalId } })
        .subscribe(data => {
          resolve();
        }, (err => {
          console.log("Error occured in deleteMailingInfo");
          reject();
        }));
    });
  }

  async switchHospital(hospital) {
    return new Promise(async (resolve, reject) => {
      try {
        await this.storageService.setSettings('HOSPITAL_ID', hospital.hospitalId);
        await this.storageService.setLocalSettings('HOSPITAL_DATA', hospital)
        this.helper.showMessage('Hospital switched successfully');
        resolve();
      }
      catch (err) {
        this.helper.showMessage('Unable to switch hospital');
        reject(err)
      }
    })
  }

  createMailingInfo(hospitalId: any, mailingInfo: any, isEdit: any) {
    let temp = {
      hospitalId: hospitalId,
      to: mailingInfo.to,
      cc: mailingInfo.cc,
      bcc: mailingInfo.bcc
    }
    Object.keys(temp).forEach((eachKey) => {
      !temp[eachKey] ? delete temp[eachKey] : '';
    })
    let mutationType = isEdit ? updateMailingInfo : createMailingInfo;


    return new Promise((resolve, reject) => {
      this.apollo.mutate({ mutation: mutationType, variables: temp })
        .take(1)
        .subscribe(data => {
          resolve();
        }, (err => {
          console.log("Error occured in createMailingInfo creation ", err);
          reject();
        }));
    });
  }

  listAllCustomUserHospitals(first: any, after: any) {
    return new Promise((resolve) => {
      this.apollo.watchQuery({ query: listAllCustomUserHospitals, variables: { first, after }, fetchPolicy: 'network-only' }).valueChanges
        .pipe(map(result => result.data['listAllCustomUserHospitals'])).subscribe(data => {
          resolve(JSON.parse(JSON.stringify(data)));
        });
    })
  }

  listMyRelations() {
    return new Promise((resolve, reject) => {
      this.apollo.watchQuery({ query: listMyRelations, fetchPolicy: 'network-only' }).valueChanges
        .pipe(map(result => result.data['listMyRelations'])).subscribe(data => {
          resolve(JSON.parse(JSON.stringify(data)));
        }, (err) => {
          reject(err)
        });
    })
  }

  getNurseListForHospital(hospitalId: string) {
    return new Promise((resolve, reject) => {
      this.apollo.watchQuery({ query: getNurseListForHospital, variables: { hospitalId }, fetchPolicy: 'network-only' }).valueChanges
        .pipe(map(result => result.data['getNurseListForHospital'])).subscribe(data => {
          resolve(JSON.parse(JSON.stringify(data)));
        }, (err) => {
          reject(err)
        });
    })
  }

  getSecretaryListForHospital(hospitalId: string) {
    return new Promise((resolve, reject) => {
      this.apollo.watchQuery({ query: getSecretaryListForHospital, variables: { hospitalId }, fetchPolicy: 'network-only' }).valueChanges
        .pipe(map(result => result.data['getSecretaryListForHospital'])).subscribe(data => {
          resolve(JSON.parse(JSON.stringify(data)));
        }, (err) => {
          reject(err)
        });
    })
  }

  getConnectedUserTypeList(connectedUserType: string) {
    return new Promise((resolve, reject) => {
      this.apollo.watchQuery({ query: getConnectedUserTypeList, variables: { connectedUserType }, fetchPolicy: 'network-only' }).valueChanges
        .pipe(map(result => result.data['queryRelationsByUserIdConnectedUserTypeIndex'])).subscribe(data => {
          resolve(JSON.parse(JSON.stringify(data)));
        }, (err) => {
          reject(err)
        });
    })
  }

  createRelations(data: any) {
    return new Promise((resolve, reject) => {
      this.apollo.mutate({ mutation: createRelations, variables: data })
        .subscribe(data => {
          resolve();
        }, (err => {
          console.log("Error occured in deleteMailingInfo");
          reject(err);
        }));
    });
  }

  deleteRelations(connectedUserAlias: string) {
    return new Promise((resolve, reject) => {
      this.apollo.mutate({ mutation: deleteRelations, variables: { connectedUserAlias } })
        .subscribe(data => {
          resolve(data);
        }, (err => {
          console.log("Error occured in hospital update", err);
          reject(err);
        }));
    });
  }

  createUserRoleSettings(roleList: any, connectedUserAlias: string) {
    return new Promise((resolve, reject) => {
      this.apollo.mutate({ mutation: createUserRoleSettings, variables: { roleList, connectedUserAlias } })
        .subscribe(data => {
          resolve(data);
        }, (err => {
          console.log("Error occured in hospital update", err);
          reject(err);
        }));
    });
  }

  updateUserRoleSettings(roleList: any, connectedUserAlias: string) {
    return new Promise((resolve, reject) => {
      this.apollo.mutate({ mutation: updateUserRoleSettings, variables: { roleList, connectedUserAlias } })
        .subscribe(data => {
          resolve(data);
        }, (err => {
          console.log("Error occured in hospital update", err);
          reject(err);
        }));
    });
  }

  deleteUserRoleSettings(connectedUserAlias: string) {
    return new Promise((resolve, reject) => {
      this.apollo.mutate({ mutation: deleteUserRoleSettings, variables: { connectedUserAlias } })
        .subscribe(data => {
          resolve(data);
        }, (err => {
          console.log("Error occured in hospital update", err);
          reject(err);
        }));
    });
  }

  getUserRoleSettingsForRelation(connectedUserAlias: string) {
    return new Promise((resolve, reject) => {
      this.apollo.watchQuery({ query: getUserRoleSettingsForRelation, variables: { connectedUserAlias }, fetchPolicy: 'network-only' }).valueChanges
        .pipe(map(result => result.data['getUserRoleSettingsForRelation'])).subscribe(data => {
          resolve(JSON.parse(JSON.stringify(data)));
        }, (err) => {
          reject(err)
        });
    })
  }

  getUserRoleSettings(connectedUserAlias: string) {
    return new Promise((resolve, reject) => {
      this.apollo.watchQuery({ query: getUserRoleSettings, variables: { connectedUserAlias }, fetchPolicy: 'network-only' }).valueChanges
        .pipe(map(result => result.data['getUserRoleSettings'])).subscribe(data => {
          resolve(JSON.parse(JSON.stringify(data)));
        }, (err) => {
          reject(err)
        });
    })
  }

  getCustomCards(hospitalId: string) {
    return new Promise((resolve, reject) => {
      this.apollo.watchQuery({ query: getCustomCards, variables: { hospitalId }, fetchPolicy: 'network-only' }).valueChanges
        .pipe(map(result => result.data['getCustomCards'])).subscribe(data => {
            console.log('getCustomCards', data)
          resolve(JSON.parse(JSON.stringify(data)));
        }, (err) => {
          reject(err)
        });
    })
  }

  getHospitalCards(hospitalId: string) {
    return new Promise((resolve, reject) => {
      this.apollo.watchQuery({ query: getHospitalCards, variables: { hospitalId }, fetchPolicy: 'network-only' }).valueChanges
        .pipe(map(result => result.data['getHospitalCards'])).subscribe(data => {
            console.log('getHospitalCards', data)
          resolve(JSON.parse(JSON.stringify(data)));
        }, (err) => {
          reject(err)
        });
    })
  }

  createCard(variables: any) {
    return new Promise((resolve, reject) => {
      this.apollo.mutate({ mutation: createCard, variables: variables })
        .subscribe(data => {
          resolve();
        }, (err => {
          console.log("Error occured in hospital update", err);
          reject(err);
        }));
    });
  }

  deleteCard(variables: any) {
    return new Promise((resolve, reject) => {
      this.apollo.mutate({ mutation: deleteCard, variables: variables })
        .subscribe(data => {
          resolve();
        }, (err => {
          console.log("Error occured in hospital delete", err);
          reject(err);
        }));
    });
  }

  getApprovalPendingCards(hospitalId: string) {
    return new Promise((resolve, reject) => {
      this.apollo.watchQuery({ query: getApprovalPendingCards, variables: { hospitalId }, fetchPolicy: 'network-only' }).valueChanges
        .pipe(map(result => result.data['getApprovalPendingCards'])).subscribe(data => {
            console.log('getHospitalCards', data)
          resolve(JSON.parse(JSON.stringify(data)));
        }, (err) => {
          reject(err)
        });
    })
  }
}
