export const environment = {
    production: false,

    region: 'us-east-1',
    identityPoolId: 'us-east-1:47d78e00-3cb1-4118-a253-4c0d2bfd6308',
    userPoolId: 'us-east-1_ASkPsomTA',
    clientId: '5t2hteso0tgltd0pmkha1gelda',

    cognito_idp_endpoint: '',
    cognito_identity_endpoint: '',
    sts_endpoint: '',
    // sts_endpoint: 'sts.us-east-1.amazonaws.com',
    dynamodb_endpoint: '',
    s3_endpoint: ''

};
