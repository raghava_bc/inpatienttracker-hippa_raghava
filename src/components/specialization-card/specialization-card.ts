import { Component, Output, EventEmitter } from '@angular/core';
import { ModalController } from 'ionic-angular';
import { Helper } from '../../providers/helper'
import { SpecializationPage } from '../../pages/settings/specialization/specialization';
import { StorageService } from '../../providers/storage-service';
import { SpecializationService } from '../../providers/specialization-service';

@Component({
  selector: 'specialization-card',
  templateUrl: 'specialization-card.html'
})

export class SpecializationCard {
  @Output() updateOptions = new EventEmitter();

  selectedSpec: any;
  specializationName: any;

  constructor(
    public helper: Helper,
    public modalCtrl: ModalController,
    public storage: StorageService,
    public service: SpecializationService
  ) {
    this.storage.getSettings('SPECIALIZATION_ID')
      .then((value: any) => {
        this.selectedSpec = value;
        if (value) {
          this.sendOption();
        }
      });
  }

  sendOption() {
    this.service.getSpecializationName(this.selectedSpec)
    .then((data:any) => {
      this.specializationName = data.name;
      this.helper.getTopLookupCodes();
      this.helper.hideLoading();
    })
    this.updateOptions.emit({
      specialization: true
    });
  }

  selectSpecialization() {
    let modal = this.modalCtrl.create(SpecializationPage, { currentSpec: this.selectedSpec });
    modal.present();

    modal.onDidDismiss((data: any) => {
      this.helper.showLoading();
      if (data) {
        this.selectedSpec = data;
        this.storage.setSettings('SPECIALIZATION_ID', this.selectedSpec);
        this.sendOption();
      } else {
        this.helper.hideLoading();
      }
    });
  }
}
