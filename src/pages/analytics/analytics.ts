import { Component, ViewChild } from '@angular/core';
import { NavParams, NavController, ToastController, Select } from 'ionic-angular';
import { Helper } from '../../providers/helper';
import { HospitalService } from '../../providers/hospitalService';
import { Storage } from '@ionic/storage';
import * as moment from 'moment';
import * as _ from 'underscore';

@Component({
  selector: 'page-analytics',
  templateUrl: 'analytics.html'
})

export class AnalyticsPage {
  @ViewChild(Select) select: Select;

  startDate: any;
  endDate: any = moment().format('');
  hospitals: any = [];
  currentHospital: any = {};
  maxDate: any = moment().format('');
  _ = _;
  analyticsResponse: any = [];

  itemTitles: any = {
    billed_diagnosis: 'Billed Diagnoses',
    billed_em_code: 'Billed E/M Codes',
    billed_procedure: 'Billed Procedures',
    unbilled_diagnosis: 'Active Diagnoses',
    unbilled_em_code: 'Active E/M Codes',
    unbilled_procedure: 'Active Procedures'
  }

  selectOptions: any = {
    title: 'Select Hospital'
  }

  constructor(
    public helper: Helper,
    public hospitalService: HospitalService,
    public params: NavParams,
    public navCtrl: NavController,
    public storage: Storage,
    public toastCtrl: ToastController,
  ) {
    this.startDate = +moment().format('DD') < 15 ? moment().subtract(1, 'month').startOf('month').format('') : moment().startOf('month').format('');
  }

  ionViewDidEnter() {
    this.init();
  }

  getAnalytics() {
    try {
      this.helper.showLoading();
      this.helper.getAnalytics(this.currentHospital.hospitalId, moment(this.startDate).format('YYYYMMDD'), moment(this.endDate).format('YYYYMMDD'))
        .then((apiRes: any) => {
          this.analyticsResponse = JSON.parse(apiRes._body);
          setTimeout(() => {
            this.helper.hideLoading();
          }, 800)
        })
        .catch((err) => {
          this.helper.hideLoading();
          console.log(err);
        });
    } catch (err) {
      console.log(err);
    }
  }

  cleanData(data: any, index: any) {
    return (data.split(',')[index]).replace(/[^\w\s]/gi, '')
  }

  async init() {
    try{
      let hospitalId = (await this.storage.get("HOSPITAL_DATA")).hospitalId;
      let self = this;
      if (hospitalId) {
        let hospitals = JSON.parse(JSON.stringify(this.hospitalService.userHospitals.getValue()));
        this.hospitals = [];
        _.each(JSON.parse(JSON.stringify(hospitals)), function(hospital: any) {
          if (hospital.isRemoved != 'true') {
            self.hospitals.push(hospital);
          }
        })
        if (hospitalId) {
          this.currentHospital = _.find(this.hospitals, { hospitalId: hospitalId });
          this.getAnalytics();
        }
      } else {
        this.helper.showMessage('Select Hospital to continue');
      }
    }catch(err){
      this.helper.hideLoading();
      console.log(err);
    }
  }

  sendReport() {
    this.helper.sendAnalytics(
      moment(this.startDate).format('DD-MMM-YYYY'),
      moment(this.endDate).format('DD-MMM-YYYY'),
      this.analyticsResponse
    );
  }

  openOptions() {
    this.select.open();
  }

}
