import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { Helper } from '../../providers/helper';
import { StorageService } from '../../providers/storage-service';
import { HospitalService } from '../../providers/hospitalService';
import * as _ from 'underscore';

@Component({
  selector: 'switch-hospital',
  templateUrl: 'switch-hospital.html'
})

export class SwitchHospital {
  hospitals: any[] = [];
  hospitalLength: any = 0;
  currentHospitalId: any;
  selectedHospitalId: any;
  selectedHospital: any = {};

  constructor(
    public helper: Helper,
    public navCtrl: NavController,
    public hospitalService: HospitalService,
    public storageService: StorageService,
  ) {
    let self = this;
    this.hospitalService.getUserHospitals()
      .then((hospitals: any) => {
        this.hospitals = [];
        this.hospitalLength = 0;
        _.each(hospitals, function(hospital: any) {
          if (hospital.hospitalId && hospital.isRemoved != 'true') {
            self.hospitalLength++;
            self.hospitals.push(hospital);
          }
        })
        return this.storageService.getSettings('HOSPITAL_DATA')
      })
      .then((hospital: any) => {
        if (hospital) {
          this.currentHospitalId = this.selectedHospitalId = hospital.hospitalId;
        }
      });
  }

  isSelected(hospitalId: any) {
    if (this.currentHospitalId == hospitalId) {
      return 'selected-hospital';
    }
  }

  selectHospital(hospital: any) {
    this.selectedHospitalId = hospital.hospitalId;
    this.selectedHospital = { ...hospital.hospitalDetail, ...{ hospitalId: this.selectedHospitalId } };
  }

  isSelectedHospital(hospitalId: any) {
    return this.selectedHospitalId == hospitalId;
  }

  isCurrentHospital() {
    return this.selectedHospitalId == this.currentHospitalId;
  }

  async switchHospital() {
    try {
      this.helper.showLoading();
      await this.storageService.setSettings('HOSPITAL_ID', this.selectedHospital.hospitalId);
      await this.storageService.setLocalSettings('HOSPITAL_DATA', this.selectedHospital)
      this.helper.hideLoading();
      this.helper.showMessage('Hospital selected successfully');
      this.navCtrl.setRoot(HomePage);
    }
    catch (err) {
      this.helper.showMessage('Unable to switch hospital');
      this.navCtrl.setRoot(HomePage);
    }
  }

}
