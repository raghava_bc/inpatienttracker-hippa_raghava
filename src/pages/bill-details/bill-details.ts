import { Component, ViewChild } from '@angular/core';
import { NavParams, ModalController, AlertController, ActionSheetController, NavController, Slides, ItemSliding, Item } from 'ionic-angular';
import { Helper } from '../../providers/helper';
import { StorageService } from '../../providers/storage-service';
import { GetNotes } from '../../queries/entities';
import { AddEntitiyPage } from '../add-entity/add-entity';
import { AddPatientPage } from '../add-patient/add-patient';
import { PatientService } from '../../providers/patient-service';
import { Keyboard } from '@ionic-native/keyboard';
import { Observable } from 'rxjs/Observable';
import { Storage } from '@ionic/storage';
import * as moment from 'moment';
import * as _ from 'underscore';
import { getPatientDetailForDr } from '../../queries/patient-info';

@Component({
  selector: 'page-bill-details',
  templateUrl: 'bill-details.html'
})

export class BillDetails {

  @ViewChild(Slides) slides: Slides;

  patient: any = {};
  items: any = { diagnosis: [], procedures: [], em_code: [] };
  groupedItems: any = { diagnosis: [], procedures: [], em_code: [] };
  recordSettings: any = {
    diagnosis: { name: 'diagnosis', key: 'icdCodes', index: 1, isEnabled: true },
    em_code: { name: 'em_code', key: 'visitCodes', index: 2, isEnabled: true },
    procedures: { name: 'procedures', key: 'cptCodes', index: 3, isEnabled: true },
  };
  hospitalId: string;
  uid: any;
  patientDataSubscription: any;
  patientEntityDetails: any;
  showFooter: boolean = true;
  apolloService: any;
  moment = moment;
  isFromHomepage: boolean;
  entitiesQueryRef: any;
  areEntitiesLoaded: any = false;
  isSavableRecordExist: boolean = false;
  aliasId:String = null;
  updateHomepageCallback: any;
  _ = _;
  currentDoctor: any = {};

  constructor(
    public actionSheetCtrl: ActionSheetController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public modal: ModalController,
    public params: NavParams,
    public helper: Helper,
    private keyboard: Keyboard,
    public patientService: PatientService,
    public storage: Storage,
    public storageService: StorageService
  ) {
    this.hospitalId = this.params.get("hospitalId");
    this.apolloService = this.patientService.apolloService;
    this.patient = this.params.data.patient;
    this.updateHomepageCallback = this.params.get("updateHomepageCallback");
    this.isFromHomepage = !!this.updateHomepageCallback;
    this.getPatientDetail();
    this.storage.get('RECORDS')
      .then((recordSettings: any) => {
        let self: any = this;
        if (!_.isEmpty(recordSettings)) {
          _.each(recordSettings, function(recordType: any) {
            self.recordSettings[recordType.name] = recordType;
          });
        }
        this.getPatientEntities();
      })
    this.storage.get('SPECIALIZATION_ID')
      .then((specializationId) => {
        if (specializationId == '6') {
          this.recordSettings.procedures.isEnabled = false;
        }
      })
    this.keyboard.onKeyboardShow()
      .subscribe(() => {
        this.showFooter = false;
      });

    // show footer on keyboard hide
    this.keyboard.onKeyboardHide()
      .subscribe(() => {
        this.showFooter = true;
      });
  }

  async getPatientDetail(){
    let currentDoctor = await this.storageService.getSettings('CURRENT_DOCTOR');
    let patientData:any = await this.patientService.getPatientDetail(this.params.data.patient.hospitalId, this.params.data.patient.patientId, currentDoctor.connectedUserAlias)
    patientData.take(1).subscribe((val) => {
      this.patient = val.getPatientDetailForDr;
    })
  }

  groupByDate(entities: any) {
    return _.groupBy(entities, (eachItem: any) => { return moment(eachItem.date, 'YYYYMMDD').format('MM/DD/YYYY') });
  }

  getCategoryTitle(type) {
    if (type == 'em_code') {
      return "E/M Code"
    }
    else if (type == 'diagnosis') {
      return "DIAGNOSES"
    }
    else {
      return type.toUpperCase();
    }
  }

  async getPatientEntities() {
    this.helper.showLoading("Loading patient details");
    let self = this;
    let patientEntities: any;
    if (!this.helper.hasPermission('SWITCH_DOCTOR')) {
      patientEntities = await this.patientService.getPatientEntities(this.entitiesQueryRef, this.patient.patientId, this.hospitalId);
    } else {
      let currentDoctor = await this.storageService.getSettings('CURRENT_DOCTOR');
      this.currentDoctor = currentDoctor && currentDoctor.connectedUserAlias ? currentDoctor : {}
      patientEntities = await this.patientService.getPatientEntitiesForDr(this.entitiesQueryRef, this.patient.patientId, this.hospitalId, this.currentDoctor.connectedUserAlias);
    }
    patientEntities
      .take(1)
      .subscribe((data) => {
        data = this.helper.hasPermission('SWITCH_DOCTOR') ? data.getDiagnosisAndBillingForPatientForDr.items : data.getDiagnosisAndBillingForPatient.items;
        data = JSON.parse(JSON.stringify(data[0] || {}));
        if (data) {
          _.keys(this.items).forEach((entityType: any) => {
            console.log(entityType);
          });
          this.patientEntityDetails = data;
          delete this.patientEntityDetails.billDetails.__typename;
          this.items = JSON.parse(JSON.stringify(this.patientEntityDetails.billDetails));
          this.patientEntityDetails.updatedOn = moment.utc().format('YYYYMMDD');
          this.areEntitiesLoaded = true;
          this.helper.hideLoading();
          this.groupItems();
        }
        else {
          this.helper.hideLoading();
        }
      })
  }

  getAllEntities() {
    let self = this;
    let sorted = _.sortBy(_.keys(this.items), function(type: any) {
      return (self.recordSettings[type] || {}).index;
    });
    return sorted
  }

  groupItems() {
    let self = this;
    delete this.items.__typename;
    console.log({...this.items});
    _.each(_.keys(this.items), (entityType: any) => {
      this.items[entityType] = _.filter(this.items[entityType], (entity)=>{ return entity.billStatus == "Ready_To_Bill" });
      this.items[entityType] = _.sortBy((this.items[entityType] || []), (eachItem: any) => { return +moment(eachItem.date).format('x') });
      this.groupedItems[entityType] = _.groupBy(_.sortBy(this.items[entityType] || [], (eachEntity) => {
        return eachEntity.name;
      }), (eachItem: any) => { return eachItem.date });
    });
  }

  ngOnDestroy() {
    this.entitiesQueryRef ? this.entitiesQueryRef.unsubscribe() : '';
  }

  async ionViewDidLoad() {
    if (this.helper.hasPermission('SWITCH_DOCTOR')) {
      this.getCurrentDoctor()
    }
    this.aliasId = await this.storage.get('aliasId')
  }

  async getCurrentDoctor() {
    this.currentDoctor = await this.storageService.getSettings('CURRENT_DOCTOR')
  }

  async billEntities(billStatus) {
    let allEntities = _.clone(JSON.parse(JSON.stringify(this.patientEntityDetails.billDetails)));
    let patientDetails = this.getPatientDetails(billStatus);
    let billableEntities: any = {};

    delete allEntities.notes;
    _.each(_.keys(allEntities), (entityType: any) => {
      _.each(allEntities[entityType], (entity) => {
        delete entity.__typename;
        if (entity.billStatus == "Ready_To_Bill") {
          billableEntities[entityType] = billableEntities[entityType] || [];
          billableEntities[entityType].push(entity);
          entity.billStatus = 'Billed';
        }
      })
    });

    let upsertObj: any = {
      billDetails: allEntities,
      hospitalId: this.hospitalId,
      patientId: patientDetails.patientId,
      updatedOn: moment.utc().format('YYYYMMDD')
    };
    console.log(upsertObj);
    try {
      this.helper.showLoading("Billing Items");
      if (this.helper.hasPermission('SWITCH_DOCTOR')) {
        upsertObj.connectedUserAlias = this.currentDoctor.connectedUserAlias
        await this.patientService.upsertPatientEntitiesForDr(upsertObj)
        let billedRecords = await this.patientService.upsertRecordsForBilling({
          hospitalId: this.patient.hospitalId,
          patientId: this.patient.patientId,
          userAlias: upsertObj.connectedUserAlias,
          isBilled: true,
        }, true)
      } else {
        await this.patientService.upsertPatientEntities(upsertObj)
      }
      await this.helper.sendRecord(patientDetails, billableEntities, billStatus.status, this.currentDoctor.connectedUserAliasName);
      this.items = { ...{ notes: this.items.notes }, ...allEntities };
      this.groupItems();
      this.helper.hideLoading();
      this.helper.showMessage("Patient billed successfully");
      this.navCtrl.pop();
    } catch (err) {
      console.log(err);
      this.helper.showMessage("Error occured while billing");
      this.helper.hideLoading();
      console.log(err);
    }
  }

  getPatientDetails(billStatus) {
    let patientInfo = { ...this.patient, ...billStatus };
    patientInfo.dateOfBirth = moment(patientInfo.dateOfBirth).format('MM/DD/YYYY');
    patientInfo.dateOfAdmission = moment(patientInfo.dateOfAdmission).format('MM/DD/YYYY');
    return patientInfo;
  }

  bill() {
    this.billEntities({ status: 'ACTIVE' });
  }

  checkForActiveEntities() {
    return (this.checkForEntities()).areActive;
  }

  checkForEntities() {
    let areActive: boolean, arePresent: boolean;
    let self = this;
    _.each(_.keys(this.items || []), function(type: any) {
      if ((self.recordSettings[type] || {}).isEnabled) {
        if (type != 'notes') {
          _.each((self.items[type] || []), function(eachEntity: any) {
            arePresent = true;
            if (eachEntity.billStatus == 'Ready_To_Bill') {
              areActive = true;
            }
          })
        }
      }
    });
    return { areActive: areActive || false, arePresent: arePresent || false };
  }

  getCurrentSlideIndex() {
    if (!this.items.notes.length || !this.slides) {
      return 0;
    }
    let index = this.slides.getActiveIndex() + 1;
    return index > this.items.notes.length ? this.items.notes.length : index;
  }

  getActiveIndex() {
    return this.slides ? this.slides.getActiveIndex() : 0;
  }

  getRecordViewPermissions(recordType: string) {
    let permisson: string = '';
    if (recordType == 'diagnosis') {
      permisson = 'VIEW_DIAGNOSIS'
    } else if (recordType == 'em_code') {
      permisson = 'VIEW_EM_CODES'
    } else if (recordType == 'procedures') {
      permisson = 'VIEW_PROCEDURES'
    }
    return permisson;
  }

  getRecordEditPermissions(recordType: string) {
    let permisson: string = '';
    if (recordType == 'diagnosis') {
      permisson = 'EDIT_DIAGNOSIS'
    } else if (recordType == 'em_code') {
      permisson = 'EDIT_EM_CODES'
    } else if (recordType == 'procedures') {
      permisson = 'EDIT_PROCEDURES'
    }
    return permisson;
  }

  getRecordCreatePermissions(recordType: string) {
    let permisson: string = '';
    if (recordType == 'diagnosis') {
      permisson = 'CREATE_DIAGNOSIS'
    } else if (recordType == 'em_code') {
      permisson = 'CREATE_EM_CODES'
    } else if (recordType == 'procedures') {
      permisson = 'CREATE_PROCEDURES'
    }
    return permisson;
  }

  getRecordDeletePermissions(recordType: string) {
    let permisson: string = '';
    if (recordType == 'diagnosis') {
      permisson = 'DELETE_DIAGNOSIS'
    } else if (recordType == 'em_code') {
      permisson = 'DELETE_EM_CODES'
    } else if (recordType == 'procedures') {
      permisson = 'DELETE_PROCEDURES'
    }
    return permisson;
  }
}
