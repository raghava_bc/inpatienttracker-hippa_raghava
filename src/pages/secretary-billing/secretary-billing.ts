import { Component, ViewChild } from '@angular/core';
import { AlertController, ActionSheetController, Content, NavController, ModalController, NavParams, Platform, Select } from 'ionic-angular';
import { HospitalService } from '../../providers/hospitalService';
import { Helper } from '../../providers/helper';
import { PatientService } from '../../providers/patient-service';
import { PatientDetailPage } from '../patient-details/patient-details';
import { Observable } from 'rxjs/Observable';
import { BillDetails } from '../bill-details/bill-details';
import { StorageService } from '../../providers/storage-service';
import { Storage } from '@ionic/storage';
import { HospitalList } from '../settings/hospital/hospital-list/hospital-list';
import { Camera, CameraOptions } from "@ionic-native/camera";
import { File } from '@ionic-native/file';

import * as moment from 'moment';
import * as _ from 'underscore';
declare const ImageCapturePlugin: any;
declare const cordova: any;

@Component({
  selector: 'page-secretary-billing',
  templateUrl: 'secretary-billing.html'
})

export class SecretaryBilling {
  @ViewChild(Content) content: Content;
  @ViewChild(Select) hospitalSelect: Select;
  @ViewChild('secDoctorList') doctorListSelect: Select;
  query = "";
  patients: any = [];
  selectOptions = {
    title: "Switch Hospital"
  };
  selectDoctorListOptions = {
    title: "Switch Doctor"
  };
  sortKey: any = "name";
  moment = moment;
  sortObject: any = { name: "NAME", medicalRecordNumber: "FIN NO", room: "ROOM NO" };
  searchKeys = ["name", "medicalRecordNumber"];
  showSearchBar: boolean = false;
  hospitalId: string;
  patientInfoQueryRef: any;
  hospitals: any = [];
  currentHospital: any = { hospitalId: null };
  selectedPatientsCount: number = 0;
  aliasId: any;
  card: any = {};
  //camera configuration
  options: CameraOptions = {
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
    correctOrientation: true,
    quality: 100,
    allowEdit: true,
    destinationType: 0,
  };
  base64data: any;
  currentDoctor: any = {};
  doctors: any[] = [];

  constructor(
    public actionSheetCtrl: ActionSheetController,
    public alertCtrl: AlertController,
    public camera: Camera,
    public file: File,
    public helper: Helper,
    private hospitalService: HospitalService,
    public modal: ModalController,
    public navCtrl: NavController,
    public navParams: NavParams,
    private patientService: PatientService,
    private platform: Platform,
    public storage: Storage,
    public storageService: StorageService
  ) {
  }

  ionViewWillEnter() {
    this.init();
  }

  abbyyRtrSdkPluginCallback(result) {
    console.log(result);
  }

  textCapture() {
    AbbyyRtrSdk.startTextCapture(this.abbyyRtrSdkPluginCallback, {
      selectableRecognitionLanguages: ["English", "French", "German", "Italian", "Polish", "PortugueseBrazilian",
        "Russian", "ChineseSimplified", "ChineseTraditional", "Japanese", "Korean", "Spanish"],
      recognitionLanguages: ["English"],

      licenseFileName: "AbbyyRtrSdk.license",
      isFlashlightVisible: true,
      stopWhenStable: true,
      areaOfInterest: ("0.9 0.38"),
      // areaOfInterest : (areaOfInterestWidth.current() + " " + areaOfInterestHeight.current()),
      isStopButtonVisible: true,
    });
  }

  getUserHospitals() {
    return new Promise(async (resolve, reject) => {
      let self = this;
      try {
        self.hospitals = [];
        self.currentHospital = (await this.storageService.getSettings("HOSPITAL_DATA")) || {};
        this.hospitalId = !_.isEmpty(this.currentHospital) ? this.currentHospital.hospitalId : (await this.storageService.getSettings("HOSPITAL_ID") || null);
        let hospitals: any = await this.hospitalService.getUserHospitals();
        await self.hospitalService.autoSelectHosptial()
        if (hospitals && hospitals.length) {
          _.each(hospitals, function(hospital: any) {
            if (hospital.isRemoved != 'true' && ((hospital.isCustom == true && hospital.approvalStatus == 'approved') || hospital.isCustom != "true")) {
              self.hospitals.push(hospital);
            }
          })
          let i = _.findIndex(this.hospitals, { hospitalId: this.hospitalId });
          if (i == -1 && hospitals.length == 1 && hospitals[0].hospitalId != this.hospitalId) {
            i = 0;
            this.hospitalId = this.hospitals[i].hospitalId;
            this.currentHospital = { ...this.hospitals[i].hospitalDetail, ...{ hospitalId: this.hospitalId } };
            this.changeHospital(this.currentHospital.hospitalId);
          }
          else {
            i = i > -1 ? i : 0;
            this.currentHospital = { ...this.hospitals[i].hospitalDetail, ...{ hospitalId: this.hospitalId } };
          }
          resolve();
        }
        else {
          this.currentHospital = {};
          this.storage.remove("HOSPITAL_DATA");
          this.storage.remove("HOSPITAL_ID");
          resolve();
        }
      } catch (err) {
        console.log(err);
        reject(err)
      }
    })
  }

  showNoHospitalAlert() {
    let alert = this.alertCtrl.create({
      title: 'Add new hospital',
      message: 'No hospitals found. Would you like to add a hospital?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Add',
          handler: () => {
            this.openHospitalList();
          }
        }
      ]
    });
    alert.present();
  }

  openHospitalList() {
    let hospitalListModal = this.modal.create(HospitalList);
    hospitalListModal.onDidDismiss((hospital) => {
      this.getUserHospitals();
    })
    hospitalListModal.present();
  }

  async init() {
    try {
      let self = this;
      this.helper.showLoading();
      await this.getUserHospitals();
      let queryRef:any ;
      if (this.hospitalId) {
        let patientInfo: any;
        if (this.helper.hasPermission('SWITCH_DOCTOR')) {
          let response: any = await this.hospitalService.listMyRelations();
          console.log('response of connected doctors', response);
          this.doctors = response;
          if ((this.doctors || []).length) {
            let currentDoctor = await this.storageService.getSettings('CURRENT_DOCTOR');
            this.currentDoctor = currentDoctor && currentDoctor.connectedUserAlias ? currentDoctor : this.doctors[0];
            await this.storageService.setLocalSettings('CURRENT_DOCTOR', this.currentDoctor)
            let response: any = await this.hospitalService.getUserRoleSettings(this.currentDoctor.connectedUserAlias);
            patientInfo = await this.patientService.getActivePatientForDr(this.patientInfoQueryRef, this.hospitalId, this.currentDoctor.connectedUserAlias);
            let permissions:any
            permissions = (response || {}).roleList && response.roleList.length ? response.roleList : [];
            console.log('permissions',permissions)
            this.helper.setPermissionData(permissions);
            let data:any = await this.patientService.queryBillingsRecordsForSecretary(queryRef, this.currentDoctor.connectedUserAlias, this.hospitalId, 20);
            data.take(1).subscribe((res) => {
              this.patients = res.queryRecordsForBillingsByHospitalIdUserAliasIndex.items || null;
              this.sortKey = this.sortKey;
              this.selectSortByOptions(this.sortKey);
              _.each(res, (eachPatient) => {
                self.selectedPatientsCount += self.getItemClass(eachPatient) == 'selected' ? 1 : 0;
              })
            });
          }
        } else {
          patientInfo = await this.patientService.getActivePatients(this.patientInfoQueryRef, this.hospitalId);
        }
          this.helper.hideLoading();
      }
      else {
        this.helper.hideLoading();
      }

      // else{
      //   let response:any = await this.hospitalService.listMyRelations()
      //   console.log('response of connected doctors',response)
      //   this.doctors = response;
      //   if((this.doctors || []).length){
      //     this.currentDoctor = await this.storageService.getSettings('CURRENT_DOCTOR') || this.doctors[0]
      //     await this.storageService.setLocalSettings('CURRENT_DOCTOR', this.currentDoctor)
      //   }
      //   this.helper.hideLoading();
      // }
    } catch (err) {
      this.helper.hideLoading();
      console.log(err);
    }
  }

  async changeHospital(hospitalId: any, refreshView?: boolean) {
    try {
      let index = _.findIndex(this.hospitals, { hospitalId: hospitalId })
      index = index > -1 ? index : 0;
      let selectedHospital = { ...this.hospitals[index].hospitalDetail, ...{ hospitalId: hospitalId } };
      this.currentHospital = selectedHospital;
      this.hospitalId = selectedHospital.hospitalId;
      this.selectedPatientsCount = 0;
      this.patients = [];
      await this.storageService.setSettings('HOSPITAL_ID', hospitalId);
      await this.storage.set("HOSPITAL_DATA", selectedHospital);
      this.init();
    } catch (err) {
      console.log(err);
    }
  }

  toggleSearchBar() {
    this.query = "";
    this.showSearchBar = !this.showSearchBar;
  }

  async viewPatientDetails(patient: any) {
    this.showSearchBar = false;
    this.navCtrl.push(BillDetails, {
      patient: patient,
      hospitalId: patient.hospitalId,
    });
  }

  updatePatients(patientObj, type) {
    console.log(this.patients);
    let index = _.findIndex(this.patients, { patientId: patientObj.patientId });
    if (index > -1 && type == "DELETE") {
      index > -1 ? this.patients.splice(index, 1) : '';
    }
    else if (index > -1) {
      this.patients[index] = patientObj;
    }
  }

  getItemClass(patient: any) {
    return patient.lastUpdated && +moment().startOf("day").format("x") <= +moment(+patient.lastUpdated).startOf("day").format("x") ? "selected" : "";
  }

  selectHospital() {
    this.query = '';
    this.showSearchBar = false;
    if (!this.hospitals.length) {
      this.showNoHospitalAlert();
    } else {
      this.hospitalSelect.open();
    }
  }

  selectDoctor() {
    if(this.currentDoctor && this.currentDoctor.connectedUserAliasName){
      this.doctorListSelect.open && this.doctorListSelect.open();
    }
  }

  async changeDoctor(connectedUserAlias: string) {
    console.log('data', connectedUserAlias)
    let index = _.findIndex(this.doctors, { connectedUserAlias });
    if (index > -1) {
      await this.storageService.setLocalSettings('CURRENT_DOCTOR', this.doctors[index])
      this.init()
    }
  }

  ngOnDestroy() {
    this.patientInfoQueryRef ? this.patientInfoQueryRef.unsubscribe() : '';
  }

  selectFilterBy(myEvent: any) {
    console.log(this.sortKey);
    let actionSheet = this.actionSheetCtrl.create({
      title: "Sort By",
      subTitle: "Patients are sorted based on your selection",
      cssClass: "sort-options",
      buttons: [
        {
          text: "NAME",
          cssClass: this.sortKey == 'name' ? 'marked-option' : 'damn',
          handler: () => {
            this.selectSortByOptions("name");
          }
        },
        {
          text: "FIN NO",
          cssClass: this.sortKey == 'medicalRecordNumber' ? 'marked-option' : 'damn',
          handler: () => {
            this.selectSortByOptions("medicalRecordNumber");
          }
        },
        {
          text: "ROOM NO",
          cssClass: this.sortKey == 'room' ? 'marked-option' : 'damn',
          handler: () => {
            this.selectSortByOptions("room");
          }
        },
        {
          text: "Cancel",
          role: "cancel"
        }
      ]
    });
    actionSheet.present();
  }

  selectSortByOptions(sortKey: any) {
    this.storageService.setLocalSettings("SORT_KEY", sortKey);
    this.sortKey = sortKey;
    this.patients = this.sortPatients(this.patients);
  }

  sortPatients(patients: any) {
    let self = this,
      temp: any = [];
    temp = _.sortBy(patients, (patient: any) => {
      return patient[self.sortKey];
    });
    return temp;
  }
}
