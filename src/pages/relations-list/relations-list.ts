import { Component, ViewChild } from '@angular/core';
import { ActionSheetController, AlertController, ModalController, NavController, NavParams, Select } from 'ionic-angular';
import { HospitalService } from '../../providers/hospitalService';
import { StorageService } from '../../providers/storage-service';
import { Helper } from '../../providers/helper';
import { CreateRelationsPage } from './create-relations/create-relations';
import { RolesManagementPage } from '../roles-management/roles-management';
import * as _ from 'underscore';

@Component({
  selector: 'page-relations-list',
  templateUrl: 'relations-list.html',
})
export class RelationsListPage {
  userHospitals: any = [];
  currentHospital: any = {};
  @ViewChild('allHospitals') hospitalSelect: Select;
  selectOptions = {
    title: "Switch Hospital"
  };
  connectedRelationsGroupList: any = {};
  _ = _;
  connectedRelations: any[] = [];
  currentRole: any;

  constructor(
    public actionSheetCtrl: ActionSheetController,
    public alertCtrl: AlertController,
    public hospitalService: HospitalService,
    public helper: Helper,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public storageService: StorageService
  ) { }

  ionViewDidLoad() {
    this.init();
  }

  async init() {
    this.helper.showLoading()
    this.currentRole = await this.storageService.getSettings('role');
    this.currentHospital = _.isEmpty(this.currentHospital) ? await this.storageService.getSettings('HOSPITAL_DATA') : this.currentHospital;
    console.log('this.currentHospital', this.currentHospital)
    this.hospitalService.getUserHospitals()
      .then((localHospitals) => {
        this.userHospitals = _.filter(localHospitals, function(local: any) {
          return (local.isRemoved != 'true' && ((local.isCustom == true && local.approvalStatus == 'approved') || local.isCustom != "true"))
        })
        this.getRealtions();
        console.log('this.userHospitals', this.userHospitals)
      })
  }

  getRealtions() {
    this.hospitalService.listMyRelations()
      .then((response: any) => {
        this.connectedRelations = _.filter(response, { hospitalId: this.currentHospital.hospitalId });
        console.log('this.connectedRelations', this.connectedRelations)
        this.connectedRelationsGroupList = _.groupBy(this.connectedRelations, function(relation) { return relation.connectedUserType })
        console.log('this.connectedRelationsGroupList', this.connectedRelationsGroupList)
        this.helper.hideLoading()
      })
      .catch((err) => {
        this.helper.hideLoading()
        console.log('error occured', err)
      })
  }

  selectHospital() {
    if (this.currentRole == 'Doctor') {
      this.hospitalSelect.open();
    }
  }

  changeHospital(hospitalId: any) {
    this.helper.showLoading()
    let index = _.findIndex(this.userHospitals, { hospitalId: hospitalId });
    this.currentHospital = { ...this.userHospitals[index].hospitalDetail, ...{ hospitalId: hospitalId } };
    this.getRealtions()
  }

  addRelation(type: string) {
    if (type == 'Nurse') {
      let nurses = this.connectedRelationsGroupList.Nurse || [];
      this.openAddRelationModal('Nurse', { selectedNurses: nurses });
    } else {
      if ((this.connectedRelationsGroupList.Secretary || []).length) {
        this.helper.showMessage('Only one Secretary can be added')
      } else {
        this.openAddRelationModal('Secretary')
      }
    }
  }

  openAddRelationModal(role: string, data?: any) {
    console.log('role', role)
    console.log('hospitalId', this.currentHospital.hospitalId)
    let navData: any = { role, hospitalId: this.currentHospital.hospitalId };
    data ? navData.selectedNurses = data : '';
    let modal = this.modalCtrl.create(CreateRelationsPage, navData);
    modal.present();
    modal.onDidDismiss(() => {
      this.getRealtions()
    })
  }

  getBadgeColor(type: string) {
    if (type == 'Doctor') {
      return 'facebook'
    } else if (type == 'Nurse') {
      return 'linkedin'
    } else {
      return 'vimeo'
    }
  }

  async detachRelation(relation: any) {
    this.helper.showLoading();
    try {
      console.log('inside the detache relation:', relation)
      // await this.hospitalService.deleteUserRoleSettings(relation.connectedUserAlias)
      await this.hospitalService.deleteRelations(relation.connectedUserAlias)
      this.helper.showMessage('Succesfully removed the relation');
      this.getRealtions();
      this.helper.hideLoading();
    } catch (err) {
      this.helper.hideLoading();
      this.helper.showMessage('Failed to remove the relation, please try again');
      console.log('err occured', err)
    }
  }

  openEditPermissionsModal(relation: any) {
    let modal = this.modalCtrl.create(RolesManagementPage, { isModal: true, title: 'Edit Permissions', isEdit: true, userType: relation.connectedUserType, relation: { connectedUserAliasId: relation.connectedUserAlias } });
    modal.present();
  }

  detachRelationConfirmation(relation: any) {
    const confirm = this.alertCtrl.create({
      title: 'Remove the relation',
      message: 'Are you sure want to remove the relation?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Continue',
          handler: () => {
            console.log('Agree clicked');
            this.detachRelation(relation)
          }
        }
      ]
    });
    confirm.present();
  }
}
