import { Component, OnDestroy, OnInit } from "@angular/core";
import { NavController, ViewController } from 'ionic-angular';
import { Helper } from '../../../providers/helper';
import { VerifyPasswordComponent } from "./verify-password/verify-password";
import { Auth } from 'aws-amplify';

@Component({
  selector: 'page-forgot-password',
  templateUrl: './forgot-password.html'
})
export class ForgotPasswordStepComponent {
  email: string;
  errorMessage: string;

  constructor(
    public viewCtrl: ViewController,
    public navCtrl: NavController,
    public helper: Helper
  ) { }

  sendOtp() {
    this.helper.showLoading();
    Auth.forgotPassword(this.email)
      .then((response: any) => {
        this.helper.hideLoading();
        this.navCtrl.push(VerifyPasswordComponent, { email: this.email });
      })
      .catch((err) => {
        this.helper.hideLoading();
        this.helper.showMessage(err.message);
        console.log('err occured', err)
      })
  }
}
