import { Component, OnDestroy, OnInit } from "@angular/core";
import { NavParams, NavController, ViewController } from 'ionic-angular';
import { LoginComponent } from "../../login/login";
import { Helper } from "../../../../providers/helper";
import { Auth } from 'aws-amplify';
const keyboard = window['Keyboard'];

@Component({
  selector: 'verify-password',
  templateUrl: './verify-password.html'
})

export class VerifyPasswordComponent implements OnInit {

  verificationCode: string = '';
  email: string = '';
  password: string = '';
  errorMessage: string = '';
  showFooter: boolean = true;

  private sub: any;

  constructor(
    public params: NavParams,
    public helper: Helper,
    public navCtrl: NavController,
    public viewCtrl: ViewController,
  ) {
    if (keyboard) {
      keyboard.hideKeyboardAccessoryBar(true);
      keyboard.disableScroll(false);
    }

    window.addEventListener('keyboardWillShow', (event) => {
      this.showFooter = false;
    });

    window.addEventListener('keyboardWillHide', () => {
      this.showFooter = true;
    });
  }

  ngOnInit() {
    this.email = this.params.get("email");
  }

  onNext() {
    this.helper.showLoading();
    Auth.forgotPasswordSubmit(this.email, this.verificationCode, this.password)
      .then((response: any) => {
        this.helper.hideLoading();
        this.helper.showMessage("Password successfully changed", 3000, 'toast-success');
        this.navCtrl.setRoot(LoginComponent);
      })
      .catch((err) => {
        this.helper.hideLoading();
        this.helper.showMessage(err.message, 3000, );
        console.log('err occured', err)
      })
  }

  resendOtp() {
    this.helper.showLoading();
    Auth.forgotPassword(this.email)
      .then((response: any) => {
        this.helper.hideLoading();
        this.helper.showMessage("Email with OTP has been sent", 3000, 'toast-success');
      })
      .catch((err) => {
        this.helper.hideLoading();
        this.helper.showMessage(err.message, 3000, );
        console.log('err occured', err)
      })
  }

}
