import { Component } from "@angular/core";
import { ModalController, NavController, ViewController } from 'ionic-angular';
import { RegistrationConfirmationComponent } from "../confirm-registration/confirm-registration";
import { Helper } from '../../../providers/helper';
import { TermsPage } from '../../../components/terms-policy/terms';
import { PolicyPage } from '../../../components/terms-policy/policy';
import { Auth } from 'aws-amplify';

export class RegistrationUser {
  name: string;
  email: string;
  phone_number: string;
  password: string;
  nickname: string;
  role: string;
}
/**
 * This component is responsible for displaying and controlling
 * the registration of the user.
 */
@Component({
  selector: 'awscognito-angular2-app',
  templateUrl: './register.html'
})
export class RegisterComponent {
  registrationUser: RegistrationUser;

  errorMessage: string;
  roles: string[] = ['Doctor', 'Nurse', 'Secretary'];

  constructor(
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public helper: Helper
  ) {
    this.onInit();
  }

  onInit() {
    this.registrationUser = new RegistrationUser();
    this.errorMessage = null;
  }

  async onRegister() {
    this.helper.showLoading();
    try {
      let response:any = await this.helper.generateUUID();
      console.log('aliasId',response)
      Auth.signUp({
        'username': this.registrationUser.email,
        'password': this.registrationUser.password,
        'attributes': {
          'email': this.registrationUser.email,
          'nickname': this.registrationUser.nickname,
          'custom:userRole': this.registrationUser.role,  // custom attribute, userRole like Secretary/Doctor/Nurse
          'custom:aliasId': response.uuid,  // custom attribute, aliasId used for mapping for permissions
          'custom:aliasName': this.registrationUser.nickname  // custom attribute, used for name resolution while mapping user roles
        }
      })
        .then(response => {
          this.helper.hideLoading();
          this.navCtrl.push(RegistrationConfirmationComponent, { email: this.registrationUser.email });
        })
        .catch(err => {
          this.helper.hideLoading();
          this.helper.showMessage(err.message, 3000);
          console.log('err occured', err);
        })
    } catch (err) {
      this.helper.hideLoading()
      this.helper.showMessage("Failed to register the user, please try again", 3000);
      console.log('err occured', err)
    }
  }

  viewTerms(link: any) {
    let page = link == 'terms' ? TermsPage : PolicyPage;
    let modal = this.modalCtrl.create(page);
    modal.present();
  }
}
