import gql from 'graphql-tag';

export const getUserHospital = gql`
query GetUserHospital($userId: String!) {
    getUserSettings(userId: $userId) {
      userId
      hospitalList {
        hospitalId
        name
        shortname
        address1
        lat
        long
      }
    }
  }
`;

export const listAllCustomUserHospitals = gql`
query listAllCustomUserHospitals($first: Int, $after: String) {
    listAllCustomUserHospitals(first: $first, after: $after) {
      items{
        hospitalId
        isCustom
        isRemoved
        hospitalDetail{
        	name
        	address1
        	address2
        	city
        	code
        	shortname
        	state
        	zipcode
        	lat
        	long
        }
        createdByEmail
        approvalStatus
        userId
      }
  	  nextToken
    }
  }
`;
