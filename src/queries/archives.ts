import gql from 'graphql-tag';

export const GetArchivedPatients = gql`
query queryArchievedPatientDetails($hospitalId:String!){
  queryArchievedPatientDetails(hospitalId:$hospitalId){
       items{
      name
      patientId
      medicalRecordNumber
      dischargeDate
      dateOfBirth
      created
      dateOfAdmission
      dischargeDate
      followUpInWeeks
      hospitalId
      referringDoctor
      room
      sex
      accountNumber
      status
      lastUpdated
    }
  }
}`;
