import gql from 'graphql-tag';

export const createSettings = gql`

mutation settings(
	$HOSPITAL_ID: String
	$AUTO_LOCK_IN_MINUTES: String
	$NOTIFICATION_TIME: String
	$PASSCODE: String
	$PDF_PASSCODE: String
	$PDF_SECURITY_ENABLED: String
	$SORT_KEY: String
	$SPECIALIZATION_ID: String
	$TOUCH_ID: String
	$USER_NAME: String
	$APP_READY: String
) {
  createUserSettings(input: {
    HOSPITAL_ID: $HOSPITAL_ID,
    AUTO_LOCK_IN_MINUTES: $AUTO_LOCK_IN_MINUTES,
    NOTIFICATION_TIME: $NOTIFICATION_TIME,
    PASSCODE: $PASSCODE,
    PDF_PASSCODE: $PDF_PASSCODE,
    PDF_SECURITY_ENABLED: $PDF_SECURITY_ENABLED,
    SORT_KEY: $SORT_KEY,
    SPECIALIZATION_ID: $SPECIALIZATION_ID,
    TOUCH_ID: $TOUCH_ID,
    USER_NAME: $USER_NAME,
		APP_READY: $APP_READY
   }) {
    HOSPITAL_ID
    AUTO_LOCK_IN_MINUTES
    NOTIFICATION_TIME
    PASSCODE
    PDF_PASSCODE
    PDF_SECURITY_ENABLED
    SORT_KEY
    SPECIALIZATION_ID
    TOUCH_ID
    USER_NAME
		APP_READY
  }
}
`;

export const updateSettings = gql`

mutation settings(
	$HOSPITAL_ID: String
	$AUTO_LOCK_IN_MINUTES: String
	$NOTIFICATION_TIME: String
	$PASSCODE: String
	$PDF_PASSCODE: String
	$PDF_SECURITY_ENABLED: String
	$SORT_KEY: String
	$SPECIALIZATION_ID: String
	$TOUCH_ID: String
	$USER_NAME: String
	$APP_READY: String
	$STRIPE_CUST_ID: String
	$DEFAULT_SOURCE_ID: String
	$TRIAL_REQUIRED: String
) {
  updateUserSettings(input: {
    HOSPITAL_ID: $HOSPITAL_ID,
    AUTO_LOCK_IN_MINUTES: $AUTO_LOCK_IN_MINUTES,
    NOTIFICATION_TIME: $NOTIFICATION_TIME,
    PASSCODE: $PASSCODE,
    PDF_PASSCODE: $PDF_PASSCODE,
    PDF_SECURITY_ENABLED: $PDF_SECURITY_ENABLED,
    SORT_KEY: $SORT_KEY,
    SPECIALIZATION_ID: $SPECIALIZATION_ID,
    TOUCH_ID: $TOUCH_ID,
    USER_NAME: $USER_NAME
		APP_READY: $APP_READY
		STRIPE_CUST_ID: $STRIPE_CUST_ID
		DEFAULT_SOURCE_ID: $DEFAULT_SOURCE_ID
		TRIAL_REQUIRED: $TRIAL_REQUIRED
   }) {
    HOSPITAL_ID
    AUTO_LOCK_IN_MINUTES
    NOTIFICATION_TIME
    PASSCODE
    PDF_PASSCODE
    PDF_SECURITY_ENABLED
    SORT_KEY
    SPECIALIZATION_ID
    TOUCH_ID
    USER_NAME
		APP_READY
		STRIPE_CUST_ID
		TRIAL_REQUIRED
  }
}
`;


export const getUserSettings = gql`
query getUserSettings {
    getUserSettings {
			PASSCODE
	 		AUTO_LOCK_IN_MINUTES
	 		HOSPITAL_ID
	 		NOTIFICATION_TIME
	 		PDF_PASSCODE
	 		PDF_SECURITY_ENABLED
	 		SORT_KEY
	 		SPECIALIZATION_ID
	 		TOUCH_ID
			APP_READY
			STRIPE_CUST_ID
			DEFAULT_SOURCE_ID
			TRIAL_REQUIRED
    }
  }
`;

export const getRolesData = gql`
query getRoles($userType:String!){
  getRoles(userType:$userType){
    userType
		supportedRoles
  }
}`;

export const listRoles = gql`
query listRoles{
  listRoles{
		userType
		supportedRoles
  }
}`;

export const listPermissions = gql`
query listPermissions{
  listPermissions{
		permissionId
		name
		sortOrder
		valueList{
			doctorControlled
			displayName
			fieldID
		}
  }
}`;

export const deleteRelations = gql`
mutation deleteRelations(
	$connectedUserAlias: String!
) {
  deleteRelations(connectedUserAlias: $connectedUserAlias){
    connectedUserAlias
    connectedUserType
    connectedUserAliasName
    hospitalId
  }
}
`;

export const createUserRoleSettings = gql`
mutation createUserRoleSettings(
	$roleList: [String],
	$connectedUserAlias: String!
) {
  createUserRoleSettings(input: {
    roleList: $roleList
	},
 	connectedUserAlias: $connectedUserAlias
) {
    connectedUserAlias
    roleList
  }
}
`;

export const updateUserRoleSettings = gql`
mutation updateUserRoleSettings(
	$roleList: [String],
	$connectedUserAlias: String!
) {
  updateUserRoleSettings(input: {
    roleList: $roleList
	},
	connectedUserAlias: $connectedUserAlias
) {
    connectedUserAlias
    roleList
  }
}
`;

export const deleteUserRoleSettings = gql`
mutation deleteUserRoleSettings(
	$connectedUserAlias: String!
) {
  deleteUserRoleSettings(input: {
    connectedUserAlias: $connectedUserAlias,
   }) {
    connectedUserAlias
    roleList
  }
}
`;

export const getUserRoleSettingsForRelation = gql`
query getUserRoleSettingsForRelation($connectedUserAlias:String!){
  getUserRoleSettingsForRelation(connectedUserAlias:$connectedUserAlias){
    connectedUserAlias
		roleList
  }
}`;

export const getUserRoleSettings = gql`
query getUserRoleSettings($connectedUserAlias:String!){
  getUserRoleSettings(connectedUserAlias:$connectedUserAlias){
    connectedUserAlias
		roleList
  }
}`;

export const getCustomCards = gql`
query getCustomCards($hospitalId:String!){
  getCustomCards(hospitalId:$hospitalId){
    approvedDate
		cardId
		createdBy
		createdByEmail
		hospitalName
		hospitalId
		s3ImageKey
		isCustom
		name
		approvalStatus
		cardDetails{
			lineIndex
      wordIndex
      name
			extractedText
		}
  }
}`;

export const getHospitalCards = gql`
query getHospitalCards($hospitalId:String!){
  getHospitalCards(hospitalId:$hospitalId){
    approvedDate
		cardId
		createdBy
		s3ImageKey
		isCustom
		name
		approvalStatus
		hospitalName
		hospitalId
		createdByEmail
		cardDetails{
			lineIndex
      wordIndex
      name
			extractedText
		}
  }
}`;

export const getApprovalPendingCards = gql`
query getApprovalPendingCards{
  getApprovalPendingCards{
    approvedDate
		cardId
		createdBy
		s3ImageKey
		isCustom
		name
		approvalStatus
		hospitalId
		createdByEmail
		hospitalName
		cardDetails{
			lineIndex
      wordIndex
      name
			extractedText
		}
  }
}`;

export const createCard = gql`
mutation createCard($hospitalId: String!, $cardId: String!, $isCustom: String, $name: String!, $hospitalName: String!, $s3ImageKey: String, $approvalStatus: String, $createdByEmail: String!, $cardDetails:[MappedDataInput]) {
  createCard(input: {
    hospitalId: $hospitalId,
    cardId: $cardId,
    isCustom: $isCustom,
		name: $name,
		hospitalName:$hospitalName,
		s3ImageKey: $s3ImageKey,
    approvalStatus: $approvalStatus,
		createdByEmail:$createdByEmail
		cardDetails:$cardDetails
  }) {
      hospitalId
      cardId
      name
			createdBy
			createdByEmail
			s3ImageKey
			approvalStatus
			cardDetails{
				lineIndex
				wordIndex
				name
				extractedText
			}
  }
}`;
export const deleteCard = gql`
mutation deleteCard($hospitalId: String!, $cardId: String!) {
  deleteCard(input: {
    hospitalId: $hospitalId,
    cardId: $cardId
  }) {
      hospitalId
      cardId
      name
			approvalStatus
  }
}`;
