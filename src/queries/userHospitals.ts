import gql from 'graphql-tag';

export const getUserHospital = gql`
query GetUserHospital {
    getUserHospitals {
      items {
        hospitalId
        cartCount
        isRemoved
        isCustom
        hospitalDetail {
          name
          code
          shortname
          address1
          address2
          city
          state
          zipcode
          lat
          long
        }
        createdByEmail
        approvalStatus
      }
    }
  }
`;

export const getMailingInfo = gql`
query getMailingInfo($hospitalId: String!){
  getMailingInfo(hospitalId:$hospitalId){
    to
    cc
    bcc
  }
}
`;

export const updateMailingInfo = gql`
mutation updateMailingInfo(
  $hospitalId: String!
  $to: String
  $cc: String
  $bcc: String
){
  updateMailingInfo(input: {
    hospitalId:$hospitalId
    to: $to
    cc: $cc
    bcc: $bcc
  }){
    to
    cc
    bcc
  }
}
`;

export const deleteUserHospitals = gql`
mutation deleteUserHospitals($hospitalId: String!) {
  deleteUserHospitals(input: {
    hospitalId: $hospitalId
  }) {
    hospitalId
  }
}
`;

export const deleteMailingInfo = gql`
mutation deleteMailingInfo($hospitalId: String!) {
  deleteMailingInfo(input: {
    hospitalId: $hospitalId
  }) {
    hospitalId
  }
}
`;

export const subscriptionToUserHospitals = gql`
subscription {
  onCreateUserHospitals {
    hospitalId
    isCustom
    isRemoved
    hospitalDetail {
      name
      code
      shortname
      address1
      address2
      city
      state
      zipcode
    }
  }
}
`;

export const createUserHospitals = gql`
mutation createUserHospitals(
  $hospitalId: String!,
  $name: String!,
  $code: String,
  $shortname: String,
  $address1: String,
  $address2: String,
  $city: String,
  $state: String,
  $zipcode: String,
  $isCustom: String,
  $lat: String,
  $long: String,
  $createdByEmail: String,
	$approvalStatus: String,
  $userAliasId: String,
	$userAliasName: String,
	$userType: String
) {
  createUserHospitals(input: {
     hospitalId: $hospitalId,
     isCustom: $isCustom,
     hospitalDetail: {
       name: $name,
       code: $code,
       shortname: $shortname,
       address1: $address1,
       address2: $address2,
       city: $city,
       state: $state,
       zipcode: $zipcode,
       lat: $lat,
       long: $long
     },
     createdByEmail: $createdByEmail,
     approvalStatus: $approvalStatus,
     userAliasId: $userAliasId,
     userAliasName: $userAliasName,
     userType: $userType
   }) {
     hospitalId
   }
}`;

export const updateUserHospitalDetail = gql`
mutation updateUserHospitals(
  $hospitalId: String!,
  $name: String,
  $code: String,
  $shortname: String,
  $address1: String,
  $address2: String,
  $city: String,
  $state: String,
  $zipcode: String,
  $isCustom: String,
  $lat: String,
  $long: String,
  $cartCount: String,
  $isCustom: String,
  $isRemoved: String,
  $createdByEmail: String,
  $approvalStatus: String
) {
  updateUserHospitals(input: {
     hospitalId: $hospitalId,
     cartCount: $cartCount,
     isCustom: $isCustom,
     isRemoved: $isRemoved,
     hospitalDetail: {
      hospitalId: $hospitalId,
      name: $name,
      code: $code,
      shortname: $shortname,
      address1: $address1,
      address2: $address2,
      city: $city,
      state: $state,
      zipcode: $zipcode,
      lat: $lat,
      long: $long
    },
    createdByEmail: $createdByEmail,
    approvalStatus: $approvalStatus
   }) {
     hospitalId
   }
}`;

export const updateUserHospital = gql`
mutation updateUserHospitals(
  $hospitalId: String!,
  $cartCount:String,
  $isCustom: String,
  $isRemoved: String
) {
  updateUserHospitals(input: {
     hospitalId: $hospitalId,
     cartCount: $cartCount,
     isCustom: $isCustom,
     isRemoved: $isRemoved
   }) {
     hospitalId
   }
}`;

export const createMailingInfo = gql`
mutation createMailingInfo(
  $hospitalId: String!,
  $to: String,
  $cc: String,
  $bcc: String,
) {
  createMailingInfo(input: {
     hospitalId: $hospitalId,
     to: $to,
     cc:$cc,
     bcc:$bcc
   }) {
     hospitalId
   }
}`;
